import logo from './logo.svg';
import './App.css';
import MyList from './components/myList'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <MyList title=" This is list of my favorite food" elements= {["pizza", "sandwiches","chicken","pineapple","spaghetti"]} />
        <MyList title=" This is list of my favorite drinks" elements= {["coca cola" , "whiskey", "vodka", "water"]} />
      </header>
    </div>
  );
}

export default App;
