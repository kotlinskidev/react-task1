const MyList = (props) => {
  return(
    <div>
      <p>{props.title}</p>
      <ul>
        {
          props.elements.map((innerText, index) => {
              return <li key={index}>{innerText}</li>;
          })
        }
        </ul>
    </div>
  );
};

export default MyList;
