const MyList = (props) => {

  const listOfComponents = props.elements.map((innerText,index) => {
    return <li key={index}>{innerText}</li>;
  });

  return(
    <div>
      <p>{props.title}</p>
      <ul>{listOfComponents}</ul>
    </div>
  );
};

export default MyList;
